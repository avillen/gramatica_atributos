%{ 
	#include <stdio.h> 
  #include <stdlib.h>	
  #include "dinero.h"
  extern int yylex();
  extern int yyerror();
  FILE * yyin;
%}			    

%union{
    Atributo atrib;
} 

%token<atrib> cifra;

%token<atrib> DOLAR;
%token<atrib> LIBRA;
%token<atrib> EURO;
%type<atrib> simbolo1;
%type<atrib> simbolo2;
%type<atrib> primero;
%type<atrib> cantidad;
%type<atrib> dinero;
%type<atrib> valor;
%type<atrib> suma;
%token TOK_MAS;

%start suma;

%%

suma: primero TOK_MAS cantidad
  {
    if(strcmp($1.tipo, $3.tipo)!=0){
      printf("error\n");
      exit(-1);
    }
    strcpy($$.tipo, $1.tipo);
    $$.valor = $1.valor+$3.valor;
    printf("%d%s\n", $$.valor, $$.tipo);
  };
primero: dinero
  {
    strcpy($$.tipo, $1.tipo);
    $$.valor = $1.valor;
  };
cantidad: dinero TOK_MAS cantidad
  {
    if(strcmp($1.tipo, $3.tipo)!=0){
      printf("error\n");
      exit(-1);
    }
    strcpy($$.tipo, $1.tipo);
    $$.valor = $1.valor + $3.valor;
  }
  | dinero
  {
    strcpy($$.tipo, $1.tipo);
    $$.valor = $1.valor;
  };
dinero: simbolo1 valor 
  {
    strcpy($$.tipo, $1.tipo);
    $$.valor = $2.valor;
  }
  | valor simbolo2
  {
    strcpy($$.tipo, $2.tipo);
    $$.valor = $1.valor;
  };
valor: cifra
  {
    $$.valor = $1.valor;
  };
simbolo1: DOLAR
  {
    strcpy($$.tipo, $1.tipo);
  }
  | LIBRA
  {
    strcpy($$.tipo, $1.tipo);
  };
simbolo2: EURO
  {
    strcpy($$.tipo, $1.tipo);
  };

%%

int main(int argc, char* argv [])
{
	if(argc != 2){
        printf("ERROR DE INVOCACION\n");
        return -1;
    }
    yyin = fopen(argv[1], "r");
        if(yyin == NULL){
           printf("ERROR EN LA APERTURA DEL FICHERO DE ENTRADA\n");
           return -1;
    }
    yyparse();
    fclose(yyin);
    return 0;
}

int yyerror(char* error)
{
	printf("%s\n", error);
	return 0;
}