%{
	#include <stdio.h> 
	#include <string.h>
	#include "dinero.h"
	#include "y.tab.h"

%}

%option noyywrap

%%

[ \n\t]	{}
[0-9]+ { yylval.atrib.valor = atoi(yytext); return cifra;}
"$"	{ strcpy(yylval.atrib.tipo, yytext); return DOLAR;}
"£"	{ strcpy(yylval.atrib.tipo, yytext); return LIBRA;}
"€"	{ strcpy(yylval.atrib.tipo, yytext); return EURO;}
"+" {return TOK_MAS;}
.	{}
%%

