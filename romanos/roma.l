%{
	#include <stdio.h> 
	#include <string.h>
	#include "roma.h"
	#include "y.tab.h"

%}

%option noyywrap

%%

[ \n\t]	{}
I	{ strcpy(yylval.atrib.lexema, yytext); yylval.atrib.valor = 1; return TOK_I;}
V	{ strcpy(yylval.atrib.lexema, yytext); yylval.atrib.valor = 5; return TOK_V;}
= {return TOK_IGUAL;}
.	{}
%%

