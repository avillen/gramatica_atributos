%{ 
	#include <stdio.h> 
  #include <stdlib.h>	
  #include "roma.h"
  extern int yylex();
  extern int yyerror();
  FILE * yyin;
%}			    

%union{
    Atributo atrib;
} 

%token<atrib> TOK_I;
%token<atrib> TOK_V;
%token TOK_IGUAL;
%type<atrib>  IList;
%type<atrib>  romano;

%start romano;

%%

romano: IList
  {
    $$.valor = $1.valor;
    fprintf(stdout, "%s: numero romano correcto, su valor es %d", $1.lexema, $1.valor);
  }
  | TOK_I TOK_V
  {
    $$.valor =  $2.valor-$1.valor;
    fprintf(stdout, "%s%s: numero romano correcto, su valor es %d", $1.lexema, $2.lexema, $$.valor);
  }
  | TOK_V IList
  {
    $$.valor =  $2.valor+$1.valor;
    fprintf(stdout, "%s%s: numero romano correcto, su valor es %d", $1.lexema, $2.lexema, $$.valor);
  }
  | romano TOK_IGUAL IList
  {
    if($1.valor == $3.valor){
      fprintf(stdout, ", coincide con la segunda cadena\n");
    } else {
      fprintf(stdout, ", no coincide con la segunda cadena\n");
    }
  };
IList: IList TOK_I
  {
    $$.valor += $2.valor;
    strcat($$.lexema, $2.lexema);
  }
  | TOK_I
  {
    $$.valor = 1;
  };
%%

int main(int argc, char* argv [])
{
	if(argc != 2){
        printf("ERROR DE INVOCACION\n");
        return -1;
    }
    yyin = fopen(argv[1], "r");
        if(yyin == NULL){
           printf("ERROR EN LA APERTURA DEL FICHERO DE ENTRADA\n");
           return -1;
    }
    yyparse();
    fclose(yyin);
    return 0;
}

int yyerror(char* error)
{
	printf("%s\n", error);
	return 0;
}