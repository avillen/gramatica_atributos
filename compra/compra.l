%{
	#include <stdio.h> 
	#include <string.h>
	#include "compra.h"
	#include "y.tab.h"

%}

%option noyywrap

%%

[ \n\t]	{}
[0-9]+.[0-9]+ { yylval.atrib.valor = atof(yytext); return TOK_VALOR; }
[a-z]+	{ strcpy(yylval.atrib.lexema, yytext); return TOK_IDENTIFICADOR;}
"["	{return TOK_CORCHETEIZQUIERDO;}
"]"	{return TOK_CORCHETEDERECHO;}
","	{return TOK_COMA;}
.	{}
%%

