%{ 
	#include <stdio.h> 
  #include <stdlib.h>	
  #include "compra.h"
  extern int yylex();
  extern int yyerror();
  FILE * yyin;
%}			    

%union{
    Atributo atrib;
} 

%token<atrib> TOK_VALOR;
%token<atrib> TOK_IDENTIFICADOR;
%token TOK_CORCHETEIZQUIERDO;
%token TOK_CORCHETEDERECHO;
%token TOK_COMA;

%type<atrib> compra;
%type<atrib> dinero;
%type<atrib> lista_compra;

%start axioma;

%%

axioma: dinero lista_compra
  {
    fprintf(stdout, "\ttotal:  %.2f\n", $2.valor);
    fprintf(stdout, "\tsaldo:  %.2f\n", $1.valor - $2.valor);    
  };
lista_compra: compra lista_compra
  {
    $$.valor = $1.valor + $2.valor;
  }
  | compra
  {
    strcpy($$.lexema, $1.lexema);
    $$.valor = $1.valor;
  };
compra: TOK_CORCHETEIZQUIERDO TOK_IDENTIFICADOR TOK_COMA TOK_VALOR TOK_CORCHETEDERECHO
  {
    strcpy($$.lexema, $2.lexema);
    $$.valor = $4.valor;    
    fprintf(stdout, "\t%s:  %.2f\n", $$.lexema, $$.valor);
  };
dinero: TOK_VALOR
  {
    $$.valor = $1.valor;
    fprintf(stdout, "\tDisponible:  %.2f\n", $$.valor);
  };

%%

int main(int argc, char* argv [])
{
	if(argc != 2){
        printf("ERROR DE INVOCACION\n");
        return -1;
    }
    yyin = fopen(argv[1], "r");
        if(yyin == NULL){
           printf("ERROR EN LA APERTURA DEL FICHERO DE ENTRADA\n");
           return -1;
    }
    yyparse();
    fclose(yyin);
    return 0;
}

int yyerror(char* error)
{
	printf("%s\n", error);
	return 0;
}