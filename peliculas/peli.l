%{
	#include <stdio.h> 
	#include <string.h>
	#include "peli.h"
	#include "y.tab.h"

%}

%option noyywrap

%%

[ \n\t]	{}
[0-9]+ { yylval.atrib.valor = atoi(yytext); return TOK_VALOR; }
[a-zA-Z_]+	{ strcpy(yylval.atrib.lexema, yytext); return TOK_IDENTIFICADOR;}
"("	{return TOK_PARENTESISIZQUIERDO;}
")"	{return TOK_PARENTESISDERECHO;}
":"	{return TOK_DOSPUNTOS;}
.	{}
%%

