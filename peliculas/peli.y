%{ 
	#include <stdio.h> 
  #include <stdlib.h>	
  #include "peli.h"
  extern int yylex();
  extern int yyerror();
  FILE * yyin;
  int num_peliculas = 0;
  char * usuario_valoracion;
  int valor_valoracion=-1;

%}			    

%union{
    Atributo atrib;
} 

%token<atrib> TOK_VALOR;
%token<atrib> TOK_IDENTIFICADOR;
%token TOK_PARENTESISIZQUIERDO;
%token TOK_PARENTESISDERECHO;
%token TOK_DOSPUNTOS;

%type<atrib> usuario;
%type<atrib> nombre_pelicula;
%type<atrib> valor;
%type<atrib> valoracion;
%type<atrib> valoraciones;
%type<atrib> pelicula;
%type<atrib> peliculas;


%start axioma;

%%

axioma: usuario peliculas 
  {
    printf("Se han valorado %d peliculas\n", num_peliculas);
    printf("La mejor valorada por %s es %s, con una valoracion de %d\n", $1.lexema, $2.lexema, $2.valor);
  };
peliculas: peliculas pelicula
  {
    if($$.valor < $2.valor) {
      $$.valor = $2.valor;
      strcpy($$.lexema, $2.lexema);
    }
  }
  | pelicula
  {
    strcpy($$.lexema, $1.lexema);
    $$.valor = $1.valor;
    num_peliculas = 1;
  };
pelicula: nombre_pelicula valoraciones
  {
    strcpy($$.lexema, $1.lexema);
    $$.valor = $2.valor;
    num_peliculas++;
  };
nombre_pelicula: TOK_IDENTIFICADOR 
  {
    strcpy($$.lexema, $1.lexema);
  }
valoraciones: valoraciones valoracion
  {
    if(strcmp($2.lexema, usuario_valoracion)==0){
        $$.valor = $2.valor;
        strcpy($$.lexema, $2.lexema); 
      }
  }
  | valoracion
  {
      if(strcmp($1.lexema, usuario_valoracion)==0){
        $$.valor = $1.valor;
        strcpy($$.lexema, $1.lexema);
      }
  };
valoracion: TOK_PARENTESISIZQUIERDO usuario TOK_DOSPUNTOS valor TOK_PARENTESISDERECHO
  {
    strcpy($$.lexema, $2.lexema);
    $$.valor = $4.valor;
  };
usuario:  TOK_IDENTIFICADOR 
  {
    strcpy($$.lexema, $1.lexema);
    if(usuario_valoracion==NULL) {
      usuario_valoracion = (char*)malloc(100*sizeof(char));
      strcpy(usuario_valoracion, $1.lexema);
    }
  };
valor: TOK_VALOR
  {
    $$.valor = $1.valor;
  };
%%

int main(int argc, char* argv [])
{
	if(argc != 2){
        printf("ERROR DE INVOCACION\n");
        return -1;
    }
    yyin = fopen(argv[1], "r");
        if(yyin == NULL){
           printf("ERROR EN LA APERTURA DEL FICHERO DE ENTRADA\n");
           return -1;
    }
    yyparse();
    fclose(yyin);
    return 0;
}

int yyerror(char* error)
{
	printf("%s\n", error);
	return 0;
}
